import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

// api imports
import client from '../api/baseUrl';

// custom components imports
import CreateInvoiceForm from '../components/CreateInvoiceForm';
import PageHeader from '../components/PageHeader';
import UseTable from '../components/UseTable';
import FilterComponent from '../components/FilterComponent';
import SelectComponent from '../components/SelectComponent';
import UseModal from '../components/UseModal';

const Invoices = () => {
  const [invoices, setInvoices] = useState(null);
  const [refresh, setRefresh] = useState(null);
  const [show, setShow] = useState(false);

  const handleModalOpen = () => {
    setShow(true);
  };

  const handleModalClose = () => {
    setShow(false);
  };

  useEffect(() => {
    getInvoices();
  }, [refresh]);

  const columns = [
    {
      name: 'Full Name',
      selector: (row) => row.fullname,
      sortable: true,
    },
    {
      name: 'Meter #',
      selector: (row) => row.meter_number,
      sortable: true,
    },
    {
      name: 'Phone #',
      selector: (row) => row.phone_number,
      sortable: true,
    },
    {
      name: 'Location',
      selector: (row) => row.location,
      sortable: true,
    },
    {
      name: 'Status',
      selector: (row) => (
        <button
          className="btn btn-sm btn-primary"
          onClick={() => handleModalOpen()}
        >
          Issue Invoice
        </button>
      ),
      sortable: true,
    },
  ];

  // get invoices
  const getInvoices = async () => {
    try {
      const { data } = await client.get('/customers');
      setInvoices(data.data);
      console.log(data.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <PageHeader title="Invoices" />

      <Container>
        <Row className="p-3 justify-content-between">
          <Col md="3">
            <SelectComponent />
          </Col>
          <Col md="5">
            <FilterComponent
              name="filterText"
              placeholder="Type name here...."
            />
          </Col>
          <Col md="12">
            <div className="form-container ">
              <h5>Customer Invoices</h5>
              {invoices && <UseTable columns={columns} data={invoices} />}
            </div>
          </Col>
        </Row>
      </Container>
      <UseModal
        show={show}
        title="Issue Invoice"
        handleClose={handleModalClose}
      >
        <div className="form-container px-3">
          <CreateInvoiceForm setRefresh={setRefresh} />
        </div>
      </UseModal>
    </div>
  );
};

export default Invoices;
