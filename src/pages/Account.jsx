import React from "react";
import { Container, Row, Col } from "react-bootstrap";

// custom component styles
import CreateCustomerForm from "../components/CreateCustomerForm";
import PageHeader from "../components/PageHeader";
import UseTable from "../components/UseTable";

const Account = () => {
  return (
    <div className="account-container">
      <PageHeader title="Accounts" />
      <Container>
        <Row>
          <Col md={4}>
            <div className="form-container py-4 px-3">
              <h5>Create Customer Account</h5>
              <CreateCustomerForm />
            </div>
          </Col>
          <Col>
            <div className="form-container py-4 px-3">
              <h5>Customer Accounts</h5>
              <UseTable />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Account;
