import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// custom components imports
import AppCard from '../components/AppCard';
import PageHeader from '../components/PageHeader';

// api clinet imports
// import client from '../api/baseUrl';

const Dashboard = () => {
  // const navigate = useNavigate();
  // const [invoices, setInvoices] = useState([]);
  // const [customers, setCustomers] = useState([]);

  // useEffect(() => {
  //   if (!localStorage.getItem('user')) {
  //     navigate('/');
  //   }

  //   getInvoices();
  //   getCustomers();
  // });

  // // get invoices
  // const getInvoices = async () => {
  //   try {
  //     const { data } = await client.get('/invoices');
  //     setInvoices(data.data);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // // get invoices
  // const getCustomers = async () => {
  //   try {
  //     const { data } = await client.get('/customers');
  //     setCustomers(data.data);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  return (
    <div className="dashboard-container">
      <PageHeader title="Dashboard" />
      <Container>
        <Row className="py-4">
          <Col md={4}>
            <AppCard count="1000" link="/customers" title="Invoices" />
          </Col>
          <Col md={4}>
            <AppCard count="50" link="/invoices" title="Paid Invoices" />
          </Col>
          <Col md={4}>
            <AppCard count="20" link="/invoices" title="Unpaid Invoices" />
          </Col>
        </Row>
        <hr />
        <Row className="py-4">
          <Col md={4}>
            <AppCard count="1000" link="/customers" title="Customers" />
          </Col>
          <Col md={4}>
            <AppCard count="50" link="/invoices" title="Location A" />
          </Col>
          <Col md={4}>
            <AppCard count="20" link="/invoices" title="Location B" />
          </Col>
          <hr className="mt-3" />
          <Col md={4}>
            <AppCard count="20" link="/invoices" title="Location B" />
          </Col>
          <Col md={4}>
            <AppCard count="20" link="/invoices" title="Location B" />
          </Col>
          <Col md={4}>
            <AppCard count="20" link="/invoices" title="Location B" />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Dashboard;
