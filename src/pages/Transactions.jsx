import React, { useState } from "react";
import { Container, Row, Col, Badge } from "react-bootstrap";

// custom components imports
import PageHeader from "../components/PageHeader";
import TransactionAlert from "../components/TransactionAlert";
import UseTable from "../components/UseTable";

const columns = [
  {
    name: "Reference #",
    selector: (row) => row.reference_no,
    sortable: true,
  },
  {
    name: "Account #",
    selector: (row) => row.account_no,
    sortable: true,
  },
  {
    name: "Invoice #",
    selector: (row) => row.invoice_no,
    sortable: true,
  },
  {
    name: "Amount",
    selector: (row) => row.amount,
    sortable: true,
  },
  {
    name: "Status",
    selector: (row) =>
      row.isPaid ? (
        <Badge bg="success">Paid</Badge>
      ) : (
        <Badge bg="warning" text="dark">
          Incomplete
        </Badge>
      ),
    sortable: true,
  },
];

const data = [
  {
    id: 1,
    reference_no: "87jhdfdfgguig",
    account_no: "Isaac Adam",
    invoice_no: "#ehrhui689",
    amount: "30000",
    isPaid: 1,
  },
  {
    id: 2,
    reference_no: "87jhdfdfgguig",
    account_no: "Isaac Adam",
    invoice_no: "#ehrhui689",
    amount: "30000",
    isPaid: 0,
  },
  {
    id: 3,
    reference_no: "87jhdfdfgguig",
    account_no: "Isaac Adam",
    invoice_no: "#ehrhui689",
    amount: "30000",
    isPaid: 1,
  },
];

const Transactions = () => {
  const [transactions, setTransactions] = useState([]);

  const handleChange = ({ selectedRows }) => {
    setTransactions(selectedRows);
    // You can set state or dispatch with something like Redux so we can use the retrieved data
    console.log("Selected Rows: ", selectedRows);
  };

  return (
    <div>
      <PageHeader title="Transactions" />

      <Container>
        <Row>
          <Col>
            <div className="form-container mt-3 px-3">
              {transactions.length && (
                <TransactionAlert transactions={transactions} />
              )}
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="form-container py-4 px-3">
              <h5>Customer Transactions</h5>
              <UseTable
                columns={columns}
                data={data}
                handleChange={handleChange}
              />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Transactions;
