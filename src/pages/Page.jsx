import React from "react";
import { Container, Row, Col } from "react-bootstrap";

// components
import Sidebar from "../components/Sidebar";
import Topbar from "../components/Topbar";

const Dashboard = ({ children }) => {
  return (
    <Container className="page-container" fluid>
      <Row>
        <Col className="p-0" md={2}>
          <Sidebar />
        </Col>
        <Col className="page-content p-0" md={10}>
          <div className="top-nav">
            <Topbar />
          </div>
          <div className="content">{children}</div>
        </Col>
      </Row>
    </Container>
  );
};

export default Dashboard;
