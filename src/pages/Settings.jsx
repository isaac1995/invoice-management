import React, { useState } from 'react';
import { Col, Container, Row, Table } from 'react-bootstrap';
import AddLocationForm from '../components/AddLocationForm';
import EditLocationForm from '../components/EditLocationForm';

import PageHeader from '../components/PageHeader';
import UseModal from '../components/UseModal';
import ViewLocationDetails from '../components/ViewLocationDetails';

const Settings = () => {
  const [addModalShow, setAddModalShow] = useState(false);
  const [editModalShow, setEditModalShow] = useState(false);
  const [viewModalShow, setViewModalShow] = useState(false);

  const handleAddLocation = () => {
    setAddModalShow(true);
  };

  const handleEditLocation = () => {
    setEditModalShow(true);
  };

  const handleViewLocation = () => {
    setViewModalShow(true);
  };

  const handleCloseModal = () => {
    setAddModalShow(false);
    setEditModalShow(false);
    setViewModalShow(false);
  };

  const handleDeleteLocation = () => {
    if (window.confirm('Are You Sure?')) {
      return console.log('Location Deleted');
    }
    return console.log('Location Not Deleted');
  };
  return (
    <div>
      <PageHeader title="Settings" />
      <Container>
        <Row className="mt-3 justify-content-around">
          <Col md={5} className="border border-secondary p-2">
            <h6 className="text-center">Location Management</h6>
            <button
              className="btn btn-sm btn-primary my-1 float-end"
              onClick={() => handleAddLocation()}
            >
              Add New
            </button>
            <Table striped bordered className="my-2 clearfix">
              <thead>
                <th>Location Name</th>
                <th>Actions</th>
              </thead>
              <tbody>
                <tr>
                  <td>Tabata Segerea</td>
                  <td>
                    <button
                      className="btn btn-sm btn-info"
                      onClick={() => handleViewLocation()}
                    >
                      View
                    </button>
                    <button
                      className="btn btn-sm btn-warning mx-2"
                      onClick={() => handleEditLocation()}
                    >
                      Edit
                    </button>
                    <button
                      className="btn btn-sm btn-danger"
                      onClick={() => handleDeleteLocation()}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
                <tr>
                  <td>Tabata Segerea</td>
                  <td>
                    <button className="btn btn-sm btn-info">View</button>
                    <button className="btn btn-sm btn-warning mx-2">
                      Edit
                    </button>
                    <button className="btn btn-sm btn-danger">Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>Tabata Segerea</td>
                  <td>
                    <button className="btn btn-sm btn-info">View</button>
                    <button className="btn btn-sm btn-warning mx-2">
                      Edit
                    </button>
                    <button className="btn btn-sm btn-danger">Delete</button>
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
          <Col md={5} className="border border-secondary p-2 ">
            <h6 className="text-center">User Management</h6>
            <button className="btn btn-sm btn-primary my-1 float-end">
              Add New
            </button>
            <Table striped bordered className="my-2 clearfix">
              <thead>
                <th>Full Name</th>
              </thead>
              <tbody>
                <tr>
                  <td>Isaac Adam</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
      <UseModal
        show={addModalShow}
        handleClose={handleCloseModal}
        title="Add New Location"
      >
        <AddLocationForm />
      </UseModal>
      <UseModal
        show={editModalShow}
        handleClose={handleCloseModal}
        title="Edit Location"
      >
        <EditLocationForm />
      </UseModal>
      <UseModal
        show={viewModalShow}
        handleClose={handleCloseModal}
        title="View Location"
      >
        <ViewLocationDetails />
      </UseModal>
    </div>
  );
};

export default Settings;
