import React, { useEffect, useState } from 'react';
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Stack,
  Badge,
  Table,
} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

// icons imports
import { MdSearch, MdAttachMoney } from 'react-icons/md';

// api imports
import client from '../api/baseUrl';

// custom components imports
import PageHeader from '../components/PageHeader';
import UseModal from '../components/UseModal';

const PayInvoice = () => {
  const navigate = useNavigate();
  const [show, setShow] = useState(false);
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    if (!localStorage.getItem('user')) {
      navigate('/');
    }

    getAllCustomersInvoices();
  }, [navigate]);

  // retrieve all customersfrom db
  const getAllCustomersInvoices = async () => {
    try {
      const { data } = await client.get('/invoices');
      setCustomers(data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleModalOpen = (data) => {
    setShow(true);
    console.log(data);
    //TODO:  pay invoice by id here check for backend
  };

  const handleModalClose = () => {
    setShow(false);
  };

  return (
    <div className="dashboard-container">
      <PageHeader title="Pay Invoice" />
      <Container>
        <Row className="pt-2">
          <Col md={4}>
            <Stack direction="horizontal" className="py-2" gap={3}>
              <Form.Control
                className="me-auto"
                placeholder="Search customer here..."
              />
              <Button variant="secondary" size="sm">
                <MdSearch />
              </Button>
            </Stack>
          </Col>
        </Row>
        <Row className="py-3">
          <Col md={12} className="">
            <Table striped>
              <thead>
                <tr>
                  <th>Meter #</th>
                  <th>Phone #</th>
                  <th>Year</th>
                  <th>Month</th>
                  <th>Unit Count</th>
                  <th>Price</th>
                  <th>Debt</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {customers?.map(
                  ({
                    meter_number,
                    phone_number,
                    year,
                    month,
                    unit_consumed,
                    required_amount,
                    debt,
                    isComplete,
                    _id,
                  }) => (
                    <tr>
                      <td>{meter_number}</td>
                      <td>{phone_number}</td>
                      <td>{year}</td>
                      <td>{month}</td>
                      <td>{unit_consumed}</td>
                      <td>{required_amount}</td>
                      <td>{debt}</td>
                      <td>
                        {!isComplete ? (
                          <Badge bg="warning" text="dark">
                            not paid
                          </Badge>
                        ) : (
                          <Badge bg="success" text="dark">
                            paid
                          </Badge>
                        )}
                      </td>
                      <td>
                        <Button
                          variant="primary"
                          onClick={() => handleModalOpen(_id)}
                          size="sm"
                        >
                          <MdAttachMoney /> Pay
                        </Button>
                      </td>
                    </tr>
                  )
                )}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
      <UseModal show={show} handleClose={handleModalClose} title="Pay Invoice">
        <Stack direction="horizontal" className="py-2" gap={3}>
          <Form.Control
            className="me-auto"
            placeholder="Enter receipt number"
          />
          <Form.Control className="me-auto" placeholder="Enter amount paid" />
          <Button variant="primary" size="sm">
            Pay
          </Button>
        </Stack>
      </UseModal>
    </div>
  );
};

export default PayInvoice;
