import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

// custom components imports
import CreateCustomerForm from '../components/CreateCustomerTable';
import PageHeader from '../components/PageHeader';
import UseTable from '../components/UseTable';
import UseModal from '../components/UseModal';

// API IMPORTS
import client from '../api/baseUrl';
import SingleCustomer from '../components/SingleCustomer';
import EditCustomerForm from '../components/EditCustomerForm';

const Customers = () => {
  const [customers, setCustomers] = useState([]);
  const [refresh, setRefresh] = useState(null);
  const [editModal, setEditModal] = useState(false);
  const [detailsModal, setDetailsModal] = useState(false);

  useEffect(() => {
    getAllCustomers();
  }, [refresh]);

  const openEditForm = () => {
    setEditModal(true);
  };

  const openDetailsModal = () => {
    setDetailsModal(true);
  };

  const handleModalClose = () => {
    setEditModal(false);
    setDetailsModal(false);
  };

  const deleteCustomer = (id) => {
    console.log('delete customer here');
  };

  const columns = [
    {
      name: 'Full Name',
      selector: (row) => row.fullname,
      sortable: true,
    },
    {
      name: 'Location',
      selector: (row) => row.location,
      sortable: true,
    },
    {
      name: 'Action',
      selector: (row) => (
        <>
          <button
            className="btn btn-sm btn-info"
            onClick={() => openDetailsModal()}
          >
            View
          </button>
          <button
            className="btn btn-sm btn-warning mx-1"
            onClick={() => openEditForm()}
          >
            Edit
          </button>
          <button
            className="btn btn-sm btn-danger"
            onClick={() => deleteCustomer('34')}
          >
            Delete
          </button>
        </>
      ),
    },
  ];

  // get all customers
  const getAllCustomers = async () => {
    try {
      const { data } = await client.get('/customers');
      setCustomers(data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleChange = ({ selectedRows }) => {
    setCustomers(selectedRows);
    // You can set state or dispatch with something like Redux so we can use the retrieved data
    console.log('Selected Rows: ', selectedRows);
  };

  return (
    <div>
      <PageHeader title="Customers" />

      <Container className="customers-container">
        <Row>
          <Col>
            <div className="form-container py-4 px-3">
              <h5>Customer Accounts</h5>
              {customers && (
                <UseTable
                  columns={columns}
                  data={customers}
                  handleChange={handleChange}
                />
              )}
            </div>
          </Col>
          <Col md={4}>
            <div className="form-container mt-4 py-2 px-3 bg-secondary">
              <h5>Create New Customer</h5>
              <CreateCustomerForm setRefresh={setRefresh} />
            </div>
          </Col>
        </Row>
      </Container>
      <UseModal
        show={detailsModal}
        handleClose={handleModalClose}
        title="View Customer Details"
      >
        <SingleCustomer />
      </UseModal>
      <UseModal
        show={editModal}
        handleClose={handleModalClose}
        title="Edit Customer Details"
      >
        <EditCustomerForm />
      </UseModal>
    </div>
  );
};

export default Customers;
