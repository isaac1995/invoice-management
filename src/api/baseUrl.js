import { create } from 'apisauce';

const client = create({
  baseURL: 'https://moyohill-api.cyclic.app/api/v1',
  headers: {
    Accept: 'application/vnd.github.v3+json',
    'Content-Type': 'application/json',
  },
});

export default client;
