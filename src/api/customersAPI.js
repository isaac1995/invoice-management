import client from './baseUrl';

export const getInvoices = async () => {
  try {
    const { data } = await client.get('/invoices');
    return data.data;
  } catch (error) {
    console.log(error);
  }
};
