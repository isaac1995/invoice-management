import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Account from './pages/Account';
import Customers from './pages/Customers';

// components imports
import Dashboard from './pages/Dashboard';
import Invoices from './pages/Invoices';
import Login from './pages/Login';
import Page from './pages/Page';
import PayInvoice from './pages/PayInvoice';
import Settings from './pages/Settings';
import Transactions from './pages/Transactions';

const CustomDashboard = () => (
  <Page>
    <Dashboard />
  </Page>
);

const CustomAccount = () => (
  <Page>
    <Account />
  </Page>
);

const CustomCustomer = () => (
  <Page>
    <Customers />
  </Page>
);

const CustomSettings = () => (
  <Page>
    <Settings />
  </Page>
);

const CustomInvoices = () => (
  <Page>
    <Invoices />
  </Page>
);

const CustomPayInvoice = () => (
  <Page>
    <PayInvoice />
  </Page>
);

const CustomTransactions = () => (
  <Page>
    <Transactions />
  </Page>
);

function App() {
  return (
    <Routes>
      <Route path="/" index element={<Login />} />
      <Route path="/dashboard" element={<CustomDashboard />} />
      <Route path="/accounts" element={<CustomAccount />} />
      <Route path="/customers" element={<CustomCustomer />} />
      <Route path="/transactions" element={<CustomTransactions />} />
      <Route path="/invoices" element={<CustomInvoices />} />
      <Route path="/pay-invoice" element={<CustomPayInvoice />} />
      <Route path="/settings" element={<CustomSettings />} />
    </Routes>
  );
}

export default App;
