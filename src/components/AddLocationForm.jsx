import React from 'react';
import { Form } from 'react-bootstrap';

const AddLocationForm = () => {
  return (
    <Form>
      <Form.Control size="sm" type="text" placeholder="Set location name" />
      <button className="btn btn-primary btn-sm mt-3 float-end">Save</button>
    </Form>
  );
};

export default AddLocationForm;
