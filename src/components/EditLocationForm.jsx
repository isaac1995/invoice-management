import React from 'react';
import { Form } from 'react-bootstrap';

const EditLocationForm = () => {
  return (
    <Form>
      <Form.Control size="sm" type="text" placeholder="Set location name" />
      <button className="btn btn-primary btn-sm mt-3 float-end">Update</button>
    </Form>
  );
};

export default EditLocationForm;
