import React from 'react';
import { Table } from 'react-bootstrap';

const SingleCustomer = () => {
  return (
    <Table striped bordered hover size="sm">
      <tbody>
        <tr>
          <td>First Name</td>
          <td>Isaac Adam</td>
        </tr>
        <tr>
          <td>Gender</td>
          <td>Male</td>
        </tr>
        <tr>
          <td>Phone Number</td>
          <td>0718793810</td>
        </tr>
        <tr>
          <td>Meter Number</td>
          <td>3765476</td>
        </tr>
        <tr>
          <td>Location</td>
          <td>Tabata</td>
        </tr>
      </tbody>
    </Table>
  );
};

export default SingleCustomer;
