import React from 'react';
import { Table } from 'react-bootstrap';

const ViewLocationDetails = () => {
  return (
    <Table striped bordered>
      <thead>
        <tr>
          <th>Location Name</th>
          <th>Tabata Segerea</th>
        </tr>
        <tr>
          <th>Total Customers</th>
          <th>300</th>
        </tr>
        <tr>
          <th>Total Debt</th>
          <th>Tsh 20000</th>
        </tr>
      </thead>
    </Table>
  );
};

export default ViewLocationDetails;
