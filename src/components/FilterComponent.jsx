import React from 'react';
import { Form } from 'react-bootstrap';

const FilterComponent = ({ value, name, type, onChange, placeholder }) => {
  return (
    <>
      <Form.Control
        className="mb-2"
        type={type || 'text'}
        name={name}
        value={value}
        onChange={onChange}
        placeholder={placeholder || ''}
        size="sm"
      />
    </>
  );
};

export default FilterComponent;
