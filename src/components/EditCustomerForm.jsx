import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const initialValues = {
  fullname: '',
  meter_number: '',
  phone_number: 0,
  gender: '',
  location: '',
};

const EditCustomerForm = () => {
  const [values, setValues] = useState(initialValues);

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
    console.log(values);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
  };

  return (
    <>
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Full Name</Form.Label>
          <Form.Control
            type="text"
            name="fullname"
            value={values.fullname}
            onChange={handleChange}
            placeholder="Enter Full Name"
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Phone Number</Form.Label>
          <Form.Control
            type="number"
            name="phone_number"
            value={values.phone_number}
            onChange={handleChange}
            placeholder="Enter Phone Number"
            minLength={10}
            maxLength={10}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Select Gender</Form.Label>
          <Form.Select
            name="gender"
            value={values.gender}
            onChange={handleChange}
            aria-label="Default select example"
            required
          >
            <option value={null}>Open this select menu</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </Form.Select>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Select Location</Form.Label>
          <Form.Select
            name="gender"
            value={values.gender}
            onChange={handleChange}
            aria-label="Default select example"
            required
          >
            <option value={null}>Open this select menu</option>
            <option value="male">Location A</option>
            <option value="female">Location B</option>
            <option value="female">Location C</option>
            <option value="female">Location D</option>
          </Form.Select>
        </Form.Group>

        <Button variant="primary" type="submit">
          Update
        </Button>
      </Form>
    </>
  );
};

export default EditCustomerForm;
