import React from "react";

const PageHeader = ({ title }) => {
  return (
    <div className="page-header">
      <h4 className="header">{title}</h4>
    </div>
  );
};

export default PageHeader;
