import React from 'react';
import { Form } from 'react-bootstrap';

const SelectComponent = () => {
  return (
    <Form.Select size="sm">
      <option>Select location</option>
      <option value="1">One</option>
      <option value="2">Two</option>
      <option value="3">Three</option>
    </Form.Select>
  );
};

export default SelectComponent;
