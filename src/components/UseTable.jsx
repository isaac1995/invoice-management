import React from 'react';
import DataTable from 'react-data-table-component';

const UseTable = ({ handleChange, columns, data, subHeaderComponentMemo }) => {
  return (
    <>
      <DataTable
        columns={columns}
        onSelectedRowsChange={handleChange}
        pagination
        data={data}
      />
    </>
  );
};

export default UseTable;
