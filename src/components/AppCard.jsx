import React from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

const AppCard = ({ title, link, count }) => {
  return (
    <Card className="my-card">
      <Card.Body>
        <div className="card-container">
          <div className="card-text">
            <Card.Title>{title}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">
              Manage {title}
            </Card.Subtitle>
          </div>
          <div className="card-value">
            <h4>{count}</h4>
          </div>
        </div>
        <div className="link-container">
          <Link className="nav-link" to={link}>
            View
          </Link>
        </div>
      </Card.Body>
    </Card>
  );
};

export default AppCard;
