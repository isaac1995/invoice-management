import React from 'react';
import { Button, ListGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// icons import
import {
  MdDashboardCustomize,
  MdGroups,
  MdSettingsApplications,
  MdAccountBalanceWallet,
  MdAttachMoney,
} from 'react-icons/md';

// image imports
import avatar from '../assets/images/avatar.png';

const links = [
  {
    id: 1,
    name: 'Dashboard',
    link: '/dashboard',
    icon: <MdDashboardCustomize />,
  },
  {
    id: 5,
    name: 'Customer',
    link: '/customers',
    icon: <MdGroups />,
  },
  {
    id: 2,
    name: 'Invoices',
    link: '/invoices',
    icon: <MdAccountBalanceWallet />,
  },
  {
    id: 3,
    name: 'Pay Invoice',
    link: '/pay-invoice',
    icon: <MdAttachMoney />,
  },
  // {
  //   id: 3,
  //   name: "Transactions",
  //   link: "/transactions",
  //   icon: <MdCompareArrows />,
  // },
  // {
  //   id: 4,
  //   name: "Account",
  //   link: "/accounts",
  //   icon: <MdAccountCircle />,
  // },

  {
    id: 6,
    name: 'Setting',
    link: '/settings',
    icon: <MdSettingsApplications />,
  },
];

const Sidebar = () => {
  return (
    <div className="sidebar">
      <img src={avatar} alt="user" />
      <ListGroup variant="flush">
        {links?.map((link) => (
          <ListGroup.Item key={link.id}>
            {link.icon}
            <Link to={link.link} className="nav-link">
              {link.name}
            </Link>
          </ListGroup.Item>
        ))}
        <ListGroup.Item>
          <Button
            onClick={() => localStorage.removeItem('user')}
            variant="danger"
          >
            Logout
          </Button>
        </ListGroup.Item>
      </ListGroup>
    </div>
  );
};

export default Sidebar;
