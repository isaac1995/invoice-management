import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

// api imports
// import client from '../api/baseUrl';

const initialValues = {
  invoice_number: '',
  due_date: new Date(),
  account_id: 0,
  unit_count: 0,
  debt: 0,
  reading_day: new Date(),
};

const CreateInvoiceForm = ({ setRefresh }) => {
  const [values, setValues] = useState(initialValues);
  const [amount, setAmount] = useState(0);

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
    console.log(values);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setAmount(() => values.unit_count * 1300);
    console.log(amount);
  };

  const year = new Date().getFullYear();
  const month = new Date().getMonth();

  return (
    <>
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Invoice Number</Form.Label>
          <Form.Control
            type="text"
            name="invoice_number"
            value={`78-${year}-${month}`}
            readOnly
            disabled
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Reading Day</Form.Label>
          <Form.Control
            name="reading_day"
            value={values.reading_day}
            onChange={handleChange}
            type="date"
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Unit Count</Form.Label>
          <Form.Control
            name="unit_count"
            value={values.unit_count}
            onChange={handleChange}
            type="number"
            min="0"
            placeholder="Enter Unit Count"
          />
          <Form.Text className="text-muted">
            Amount {amount}
            <button
              className="btn btn-sm my-btn"
              onClick={() => setAmount(values.unit_count * 1300)}
            >
              Compute
            </button>
          </Form.Text>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Due Date</Form.Label>
          <Form.Control
            onChange={handleChange}
            name="due_date"
            value={values.due_date}
            type="date"
          />
        </Form.Group>
        {/* TODO: Remove here after use */}
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Debt</Form.Label>
          <Form.Control
            name="debt"
            value={values.debt}
            onChange={handleChange}
            type="number"
            min="0"
          />
        </Form.Group>
        <Button variant="primary" className="btn-sm" type="submit">
          Issue Invoice
        </Button>
      </Form>
    </>
  );
};

export default CreateInvoiceForm;
